package pl.bestify.bestifydriver

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.context.ModuleDefinition
import org.koin.dsl.module.module
import pl.bestify.bestifydriver.networking.BestifyApi
import pl.bestify.bestifydriver.networking.BestifyNetworking
import pl.bestify.bestifydriver.networking.RetrofitBestifyNetworking
import retrofit2.Retrofit

object BestifyModule {

    val module = module {
        singles()
    }

    private fun ModuleDefinition.singles() {
        single { get<Retrofit>().create(BestifyApi::class.java) }
        single { RetrofitBestifyNetworking(get()) as BestifyNetworking }
        viewModel { MainViewModel(get()) }
    }
}