package pl.bestify.bestifydriver

import android.app.Application
import org.koin.android.ext.android.startKoin
import pl.bestify.bestifydriver.networking.NetworkModule

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        val modules = listOf(NetworkModule(), BestifyModule.module)
        startKoin(this, modules)
    }
}