package pl.bestify.bestifydriver

object ServiceBridge {
    var times: MutableList<Int> = mutableListOf()
    var locationServiceResponse: ((ServiceResponse) -> Unit)? = null

    fun onNewLocation(time: Int, latitude: Double, longitude: Double) {
        times.add(time)
        val average = times.average()
        val serviceResponse = ServiceResponse(times.size, average, latitude, longitude)
        locationServiceResponse?.invoke(serviceResponse)
    }
}

class ServiceResponse(val timesSize: Int, val average: Double, val latitude: Double, val longitude: Double)