package pl.bestify.bestifydriver

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import pl.bestify.bestifydriver.networking.BestifyNetworking
import pl.bestify.bestifydriver.networking.model.DriverPosition
import pl.bestify.bestifydriver.networking.model.SetDriverPositionResponse
import kotlin.coroutines.CoroutineContext

class MainViewModel(private val bestifyNetworking: BestifyNetworking) : ViewModel(), CoroutineScope {

    private var tempTotalSuccessRequests = 0
    private var tempTotalFailureRequests = 0

    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.Main

    private val mutableSetPositionResult = MutableLiveData<TestRequests>()
    val setPositionResult: LiveData<TestRequests> = mutableSetPositionResult

    fun sendDriverPosition(driverPosition: DriverPosition) {
        launch(Dispatchers.IO) {
            val isSuccess = when (bestifyNetworking.setPosition(listOf(driverPosition))) {
                is SetDriverPositionResponse.Success -> true
                is SetDriverPositionResponse.Failure -> false
                is SetDriverPositionResponse.ServerError -> false
            }

            if (isSuccess) {
                tempTotalSuccessRequests++
            } else {
                tempTotalFailureRequests++
            }

            mutableSetPositionResult.postValue(TestRequests(tempTotalSuccessRequests, tempTotalFailureRequests))
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    class TestRequests(val success: Int, val failure: Int)
}