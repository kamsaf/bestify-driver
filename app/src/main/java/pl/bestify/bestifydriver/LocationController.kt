package pl.bestify.bestifydriver

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult

class LocationController(context: Context, locationResponse: (Location) -> Unit) {

    private val fusedLocationProviderClient = FusedLocationProviderClient(context)

    private val locationRequest = LocationRequest().apply {
        this.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        this.interval = 750
        this.fastestInterval = 750
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            locationResult?.lastLocation?.let {
                locationResponse(it)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun startObservingLocation() {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    fun stopObservingLocation() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }
}