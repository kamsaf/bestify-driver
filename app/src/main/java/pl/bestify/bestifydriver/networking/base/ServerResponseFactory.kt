package pl.bestify.bestifydriver.networking.base

import okhttp3.ResponseBody
import retrofit2.Call

abstract class ServerResponseFactory<T> {
    abstract fun createSuccessResponse(json: String): T
    abstract fun createServerError(errorJson: String): T
    abstract fun createRequestError(failure: Throwable): T

    inline fun executeAndParseCall(callCreator: () -> Call<ResponseBody>): T {
        return try {

            val call = callCreator.invoke()
            val response = call.execute()

            if (response.isSuccessful) {
                val body = response.body()?.string()!!
                createSuccessResponse(body)
            } else {
                val errorText = response.errorBody()?.string() ?: ""
                createServerError(errorText)
            }
        } catch (t: Throwable) {
            createRequestError(t)
        }
    }
}