package pl.bestify.bestifydriver.networking

import okhttp3.ResponseBody
import pl.bestify.bestifydriver.networking.model.DriverPosition
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface BestifyApi {

    @POST("v1/set_position")
    fun setPosition(@Body driverPositions: List<DriverPosition>): Call<ResponseBody>
}