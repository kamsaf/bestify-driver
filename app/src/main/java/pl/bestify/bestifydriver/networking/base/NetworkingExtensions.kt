package pl.bestify.bestifydriver.networking.base

import com.squareup.moshi.Moshi

inline fun <reified T> fromJson(json: String): T {
    val moshi = Moshi.Builder().build()
    val jsonAdapter = moshi.adapter<T>(T::class.java)
    return jsonAdapter.fromJson(json)!!
}