package pl.bestify.bestifydriver.networking

import pl.bestify.bestifydriver.networking.model.DriverPosition
import pl.bestify.bestifydriver.networking.model.SetDriverPositionResponse
import pl.bestify.bestifydriver.networking.model.SetDriverPositionResponseFactory

class RetrofitBestifyNetworking(private val bestifyApi: BestifyApi) : BestifyNetworking {

    override suspend fun setPosition(driverPositions: List<DriverPosition>): SetDriverPositionResponse {
        return SetDriverPositionResponseFactory().executeAndParseCall {
            bestifyApi.setPosition(driverPositions)
        }
    }
}