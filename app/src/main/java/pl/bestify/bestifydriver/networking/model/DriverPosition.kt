package pl.bestify.bestifydriver.networking.model

typealias Second = Int
data class DriverPosition(val phone: String, val time: Second, val latitude: String, val longitude: String)