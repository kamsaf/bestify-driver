package pl.bestify.bestifydriver.networking.model.response

data class SetPositionSuccessResponse(val success: Int, val total: Int)