package pl.bestify.bestifydriver.networking.model.response

data class SetPositionErrorResponse(val error: String)