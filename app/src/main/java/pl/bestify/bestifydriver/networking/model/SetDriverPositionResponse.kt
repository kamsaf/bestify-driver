package pl.bestify.bestifydriver.networking.model

import pl.bestify.bestifydriver.networking.base.ServerResponseFactory
import pl.bestify.bestifydriver.networking.base.fromJson
import pl.bestify.bestifydriver.networking.model.response.SetPositionErrorResponse
import pl.bestify.bestifydriver.networking.model.response.SetPositionSuccessResponse

sealed class SetDriverPositionResponse {
    class Success(val successResponse: SetPositionSuccessResponse) : SetDriverPositionResponse()
    class Failure(val errorResponse: SetPositionErrorResponse) : SetDriverPositionResponse()
    data class ServerError(val throwable: Throwable) : SetDriverPositionResponse()
}

class SetDriverPositionResponseFactory : ServerResponseFactory<SetDriverPositionResponse>() {
    override fun createSuccessResponse(json: String): SetDriverPositionResponse {
        return SetDriverPositionResponse.Success(fromJson(json))
    }

    override fun createServerError(errorJson: String): SetDriverPositionResponse {
        return SetDriverPositionResponse.Failure(fromJson(errorJson))
    }

    override fun createRequestError(failure: Throwable): SetDriverPositionResponse {
        return SetDriverPositionResponse.ServerError(failure)
    }

}