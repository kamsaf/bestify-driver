package pl.bestify.bestifydriver.networking

import pl.bestify.bestifydriver.networking.model.DriverPosition
import pl.bestify.bestifydriver.networking.model.SetDriverPositionResponse

interface BestifyNetworking {
    suspend fun setPosition(driverPositions: List<DriverPosition>): SetDriverPositionResponse
}