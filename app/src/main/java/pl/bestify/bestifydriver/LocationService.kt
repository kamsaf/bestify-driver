package pl.bestify.bestifydriver

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleService

class LocationService : LifecycleService() {

    private var lastTime: Long? = null

    private val locationServiceTag = "Location Service"
    private val locationController by lazy {
        LocationController(this) {
            val currentTime = System.currentTimeMillis()

            val time = if (lastTime == null) {
                0
            } else {
                currentTime - lastTime!!
            }

            lastTime = currentTime
            if (time != 0L) {
                ServiceBridge.onNewLocation(time.toInt(), it.latitude, it.longitude)
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("location_service", locationServiceTag)
            } else {
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .setContentTitle(locationServiceTag)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()

        startForeground(4823, notification)

        locationController.startObservingLocation()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val existingChannel = service.getNotificationChannel(channelId)
        if (existingChannel == null) {
            val chan = NotificationChannel(
                channelId,
                channelName, NotificationManager.IMPORTANCE_NONE
            )
            chan.lightColor = Color.BLACK
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

            service.createNotificationChannel(chan)
        }

        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()
        locationController.stopObservingLocation()
    }

    companion object {
        fun startService(context: Context) {
            ContextCompat.startForegroundService(context, Intent(context, LocationService::class.java))
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, LocationService::class.java))
        }
    }
}