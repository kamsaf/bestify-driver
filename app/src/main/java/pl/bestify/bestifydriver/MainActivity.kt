package pl.bestify.bestifydriver

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.bestify.bestifydriver.networking.model.DriverPosition

class MainActivity : AppCompatActivity() {

    private val permissionRequestCode = 3423
    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.setPositionResult.observe(this, Observer {
            requests.text = "Requesty: udane: ${it.success}, nieudane: ${it.failure}"
        })

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                permissionRequestCode
            )
        } else {
            startService()
        }

        ServiceBridge.locationServiceResponse = { serviceResponse ->

            val totalValues = serviceResponse.timesSize
            val average = serviceResponse.average

            info_text.text = "Liczba odczytów: $totalValues, średnia: $average"

            val timestamp = System.currentTimeMillis() / 1000
            val testPhoneNumber = "123456789"
            val driverPosition = DriverPosition(
                testPhoneNumber,
                timestamp.toInt(),
                serviceResponse.latitude.toString(),
                serviceResponse.longitude.toString()
            )
            viewModel.sendDriverPosition(driverPosition)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCode && (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED)) {
            startService()
        }
    }

    private fun startService() {
        LocationService.startService(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        LocationService.stopService(this)
    }
}
